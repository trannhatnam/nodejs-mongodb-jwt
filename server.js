const express = require("express");
const cors = require("cors");

const app = express();

var corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

const db = require("./app/models");
const Role = db.role;
console.log(db.url);
db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to the database!");
        initial()
            .then(() => {
                console.log("Initialization completed successfully.");
            })
            .catch(err => {
                console.error("Initialization failed:", err);
            });
    })
    .catch(err => {
        console.log("Cannot connect to the database!", err);
        process.exit();
    });


function initial() {
    return Role.estimatedDocumentCount()
        .then(count => {
            if (count === 0) {
                const roles = [
                    { name: "user" },
                    { name: "moderator" },
                    { name: "admin" }
                ];

                const rolePromises = roles.map(role => {
                    return new Role(role).save()
                        .then(() => {
                            console.log(`added '${role.name}' to roles collection`);
                        })
                        .catch(err => {
                            console.log("error", err);
                        });
                });

                return Promise.all(rolePromises);
            } else {
                return Promise.resolve(); // Nothing to do if count > 0
            }
        })
        .catch(err => {
            console.log("Error:", err);
        });
}

require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);

// simple route
app.get("/", (req, res) => {
    res.json({ message: "Welcome to node application." });
});


// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});